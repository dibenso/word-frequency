# Word Frequency

A next.js web application that displays the frequencies of words given to it. This accepts text through a text box and processes the frequencies in real time. This results of the frequencies includes a list of the words and their respective occurences as well as a visual bar chart that shows the top 8 most frequently used words. Why? Mostly for fun but also to help when writing to decide which words are used too much. 

### Installing and starting server

```sh
git clone https://github.com/dibenso/word-frequency.git
cd word-frequency
npm install
npm run build
npm start
```

## Built With

* [next.js](https://github.com/zeit/next.js/) - Framework for server-rendered React applications.

## Contributing

1. **Fork** the repo
2. **Clone** the project to your own machine
3. **Commit** changes to your own branch
4. **Push** your work back up to your fork
5. Submit a **Pull request** so that we can review your changes

NOTE: Be sure to merge the latest from "upstream" before making a pull request!

## Authors

* [**Dillon Benson**](https://github.com/dibenso)

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://github.com/dibenso/word-frequency/blob/master/LICENSE.md) file for details
