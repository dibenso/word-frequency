import fetch from 'isomorphic-unfetch'
import { ADD_AND_SORT, TOGGLE_TEXT, SWITCH_INPUT_CONTROL,
         FETCHING_PAGE, FETCH_PAGE_SUCCESS, FETCH_PAGE_FAIL,
         SERVER_URL, SERVER_PORT, TOGGLE_COMMON_WORDS,
         COMMON_WORDS } from '../constants'

export const addAndSort = text => (dispatch, getState) => {
  const words = text.toLowerCase().split(' ')
  const { excludeCommonWords } = getState()
  const sortedFrequencies = sortFrequencies(words, excludeCommonWords)

  return dispatch({ type: ADD_AND_SORT, frequencies: sortedFrequencies, text })
}

export const toggleText = () => dispatch => {
  return dispatch({ type: TOGGLE_TEXT })
}

export const toggleCommonWords = () => (dispatch, getState) => {
  dispatch({ type: TOGGLE_COMMON_WORDS })
  const { text } = getState()
  dispatch(addAndSort(text))
}

export const switchInputControl = control => dispatch => {
  return dispatch({ type: SWITCH_INPUT_CONTROL, control })
}

export const fetchPage = url => dispatch => {
  dispatch({ type: FETCHING_PAGE })

  const fetchUrl = `${SERVER_URL}:${SERVER_PORT}/fetch?url=${url}`

  fetch(fetchUrl)
  .then(res => {
    if(res.ok)
      return res.json()
    else
      throw "Request error"
  })
  .then(json => {
    const { text } = json

    dispatch(fetchPageSuccess())
    dispatch(addAndSort(text))
  })
  .catch(error => {
    dispatch(fetchPageFail(`Unable to contact ${fetchUrl}`))
  })
}

export const fetchPageSuccess = () => {
  return { type: FETCH_PAGE_SUCCESS }
}

export const fetchPageFail = errorMessage => {
  return { type: FETCH_PAGE_FAIL, errorMessage }
}

const sortFrequencies = (words, excludeCommonWords) => {
  const frequencies = {}
  const sortedFrequencies = {}

  words.map((word, i) => {
    const lettersOnlyWord = word.match(/'?[a-zA-Z][a-zA-Z']*(?:-\w+)*'?/)

    if(lettersOnlyWord) {
      if(excludeCommonWords && COMMON_WORDS.includes(lettersOnlyWord[0]))
        return
      else if(frequencies[lettersOnlyWord])
        frequencies[lettersOnlyWord] = ++frequencies[lettersOnlyWord]
      else
        frequencies[lettersOnlyWord] = 1
    }
  })

  const sortedFrequenciesKeys = Object.keys(frequencies).sort((a, b) => {
    return frequencies[b] - frequencies[a]
  })

  sortedFrequenciesKeys.forEach(key => {
    sortedFrequencies[key] = frequencies[key]
  })

  return sortedFrequencies
}
