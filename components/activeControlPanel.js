import React from 'react'
import KeyboardLogo from 'react-icons/lib/md/keyboard'
import LinkLogo from 'react-icons/lib/md/link'
import MicLogo from 'react-icons/lib/md/mic'
import { KEYBOARD, URL, MIC } from '../constants'

const ActiveControlPanel = props => {
  const { control, active, handleClick } = props

  switch(control) {
    case KEYBOARD:
      if(active === KEYBOARD) {
        return (
          <div className="col-4 active-control" style={{backgroundColor: "#808080"}}>
            <h6>Type</h6>
            <KeyboardLogo size={70} />
          </div>
        )
      }
      else {
        return (
          <div className="col-4" onClick={handleClick}>
            <a className="clickable-control" href="#" id={KEYBOARD}>
               Type
              <KeyboardLogo size={70} />
            </a>
          </div>
        )
      }
    case URL:
      if(active === URL) {
        return (
          <div className="col-4 active-control" style={{backgroundColor: "#808080"}}>
            <h6>URL</h6>
            <LinkLogo size={70} />
          </div>
        )
      }
      else {
        return (
          <div className="col-4" onClick={handleClick}>
            <a className="clickable-control" href="#" id={URL}>
               URL
              <LinkLogo size={70} />
            </a>
          </div>
        )
      }
    case MIC:
      if(active === MIC) {
        return (
          <div className="col-4 active-control" style={{backgroundColor: "#808080"}}>
            <h6>Speak</h6>
            <MicLogo size={70} />
          </div>
        )
      }
      else {
        return (
          <div className="col-4" onClick={handleClick}>
            <a className="clickable-control" href="#" id={MIC}>
               Speak
              <MicLogo size={70} />
            </a>
          </div>
        )
      }
    default:
      return (
          <div className="col-4">
          </div>
        )
  }
}

export default ActiveControlPanel
