import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ResponsiveContainer, BarChart,
         Bar, CartesianGrid, XAxis,
         YAxis, Tooltip, Label } from 'recharts'
import { BAR_COLORS } from '../constants'

class Chart extends Component {
  render() {
    const { frequencies } = this.props
    let data = []

    Object.keys(frequencies).map((key, i) => {
      if(i < 8) {
        const barData = {
          fill: BAR_COLORS[i],
          name: key,
          payload: {
            name: key,
            occurences: frequencies[key]
          },
          stroke: "#fff",
          occurences: frequencies[key]
        }

        data.push(barData)
      }
    })

    return (
      <div>
        <ResponsiveContainer width="100%" height={300}>
          <BarChart width={450} height={200} data={data}
                    margin={{bottom: 20, left: 10, right: 10}}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis label={{value: "Word", position: "bottom"}}
                   dataKey="name" />
            <XAxis dataKey="name">
              <Label value="Words" offset={0} position="insideBottom" />
            </XAxis>
            <YAxis label={{value: "Occurences", angle: -90, position: "insideLeft"}}
                   allowDecimals={false} />
            <Tooltip />
            <Bar dataKey="occurences" fill="#8884d8" />
          </BarChart>
        </ResponsiveContainer>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { frequencies } = state

  return { frequencies }
}

export default connect(mapStateToProps)(Chart)
