import React, { Component } from 'react'
import { connect } from 'react-redux'

class Results extends Component {
  render() {
    const { frequencies } = this.props

    return (
      <div>
        <h1>Results</h1>
        <ol>
          {Object.keys(frequencies).map((word, i) => {
            return <li key={word}>{`${word}: ${frequencies[word]}`}</li>
          })}
        </ol>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { frequencies } = state

  return { frequencies }
}

export default connect(mapStateToProps)(Results)
