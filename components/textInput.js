import React, {Component} from 'react'
import { connect } from 'react-redux'
import { addAndSort, fetchPage, fetchPageFail } from '../actions'
import { KEYBOARD, URL, MIC } from '../constants'

class TextInput extends Component {
  handleChange = event => {
    const { dispatch } = this.props
    const text = event.target.value

    dispatch(addAndSort(text))
  }

  handleUrlInput = event => {
    const key = event.which || event.keyCode
    const url = event.target.value
    const urlExists = url && url !== ''

    if(key === 13 && urlExists) {
      console.log('Enter hit...')
      const { dispatch } = this.props

      if(isURL(url))
        dispatch(fetchPage(url))
      else
        dispatch(fetchPageFail(`'${url}' is not a valid URL`))
    }
  }

  render() {
    const { activeInputControl, text } = this.props

    switch(activeInputControl) {
      case KEYBOARD:
        return (
          <div className="form-group">
            <label htmlFor="input-text">Enter text below:</label>
            <textarea className="form-control text-input" rows="5" id="input-text"
                      onChange={this.handleChange} placeholder="Enter text here..." value={text}>
            </textarea>
          </div>
        )
      case URL:
        return (
          <div className="form-group">
            <label htmlFor="url-input">Enter URL below:</label>
            <input className="form-control text-input" id="url-input"
                   onKeyUp={this.handleUrlInput} placeholder="Enter URL here...">
            </input>
          </div>
        )
      case MIC:
        return (
          <div>
            <h2>Start speaking...</h2>
          </div>
        )
      default:
        return (
          <div>
          </div>
        )
    }
  }
}

function mapStateToProps(state) {
  const { activeInputControl, text } = state

  return { activeInputControl, text }
}

function isURL(str) {
  var urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$'
  var url = new RegExp(urlRegex, 'i')
  return str.length < 2083 && url.test(str)
}

export default connect(mapStateToProps)(TextInput)