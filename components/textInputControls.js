import React, { Component } from 'react'
import { connect } from 'react-redux'
import ActiveControlPanel from './activeControlPanel'
import { switchInputControl, toggleCommonWords } from '../actions'
import { KEYBOARD, URL, MIC } from '../constants'

class TextInputControls extends Component {
  handleClick = event => {
    const { dispatch } = this.props
    let selectedControl = event.target

    // keep getting parent until we find an id attribute
    while(!selectedControl.id) {
      selectedControl = selectedControl.parentNode
    }

    dispatch(switchInputControl(selectedControl.id))
  }

  handleToggle = event => {
    const { dispatch } = this.props

    dispatch(toggleCommonWords())
  }

  render() {
    const { activeInputControl, excludeCommonWords } = this.props

    return (
      <div>
        <div className="row input-control">
          <CommonWordsToggle excludeCommonWords={excludeCommonWords} handleToggle={this.handleToggle} />
          <br />
        </div>
        <div className="row">
          <ActiveControlPanel control={KEYBOARD} active={activeInputControl} handleClick={this.handleClick} />
          <ActiveControlPanel control={URL} active={activeInputControl} handleClick={this.handleClick} />
          <ActiveControlPanel control={MIC} active={activeInputControl} handleClick={this.handleClick} /> 
        </div>
      </div>
    )
  }
}

const CommonWordsToggle = ({ excludeCommonWords, handleToggle }) => {
  if(excludeCommonWords)
    return (
      <button type="button" className="btn btn-primary" data-toggle="button"
              aria-pressed="on" autoComplete="off" onClick={handleToggle}>
        Include common words
      </button>
    )
  else
    return (
      <button type="button" className="btn btn-primary" data-toggle="button"
              aria-pressed="false" autoComplete="off" onClick={handleToggle}>
        Exclude common words
      </button>
    )
}

function mapStateToProps(state) {
  const { activeInputControl, excludeCommonWords } = state

  return { activeInputControl, excludeCommonWords }
}

export default connect(mapStateToProps)(TextInputControls)
