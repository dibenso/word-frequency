import React, { Component } from 'react'
import { connect } from 'react-redux'

class TextOutput extends Component {
  render() {
    const { text, showText } = this.props

    return (
      <div>
        {showText && (<p>{text}</p>)}
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { text, showText } = state

  return { text, showText }
}

export default connect(mapStateToProps)(TextOutput)
