import React, { Component } from 'react'
import { connect } from 'react-redux'
import { toggleText } from '../actions'

class TextOutputControls extends Component {
  handleClick = event => {
    const { dispatch } = this.props

    dispatch(toggleText())
  }

  render() {
    const { showText } = this.props
    const buttonText = showText ? 'Hide Text' : 'Show Text'

    return (
      <button type="button" className="btn btn-primary" data-toggle="button"
              aria-pressed="false" autoComplete="off" onClick={this.handleClick}>
        {buttonText}
      </button>
    )
  }
}

function mapStateToProps(state) {
  const { showText } = state

  return { showText }
}

export default connect(mapStateToProps)(TextOutputControls)
