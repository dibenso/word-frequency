// action types
export const ADD_AND_SORT = 'ADD_AND_SORT'
export const TOGGLE_TEXT = 'TOGGLE_TEXT'
export const SWITCH_INPUT_CONTROL = 'SWITCH_INPUT_CONTROL'
export const FETCHING_PAGE = 'FETCHING_PAGE'
export const FETCH_PAGE_SUCCESS = 'FETCH_PAGE_SUCCESS'
export const FETCH_PAGE_FAIL = 'FETCH_PAGE_FAIL'
export const TOGGLE_COMMON_WORDS = 'TOGGLE_COMMON_WORDS'

// chart colors
export const BAR_COLORS = [
  "#ff0000",      // red
  "#ff5400",      // orange
  "#ffff00",      // yellow
  "#21ff00",      // green
  "#0077ff",      // blue
  "#2600ff",      // indigo
  "#ff00fa",      // violet
  "#840230"       // purple 
]

// top 20 most common words
export const COMMON_WORDS = [
  'the',
  'be',
  'to',
  'of',
  'and',
  'a',
  'in',
  'that',
  'have',
  'i',
  'it',
  'for',
  'not',
  'on',
  'with',
  'he',
  'as',
  'you',
  'do',
  'at',
  'this'
]

// ui controls
export const KEYBOARD = 'KEYBOARD'
export const URL = 'URL'
export const MIC = 'MIC'

// server constants
export const SERVER_URL = 'http://localhost'
export const SERVER_PORT = 3000
