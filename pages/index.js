import React from 'react'
import { connect } from 'react-redux'
import Header from '../components/header'
import TextInput from '../components/textInput'
import TextOutput from '../components/textOutput'
import Results from '../components/results'
import Chart from '../components/chart'
import TextOutputControls from '../components/textOutputControls'
import TextInputControls from '../components/textInputControls'

class Index extends React.Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-sm-12 text-center">
            <Header />
          </div>
          <div className="col-sm-6 text-center">
            <TextInput />
            <TextInputControls />
          </div>
          <div className="col-sm-6 text-center">
            <h2>Output</h2>
            <TextOutputControls />
            <TextOutput />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 text-center">
            <Results />
          </div>
          <div className="col-sm-6 text-center">
            <h2>Chart</h2>
            <Chart />
          </div>
        </div>
      </div>
    )
  }
}

export default connect()(Index)
