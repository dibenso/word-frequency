const express = require('express')
const fetch = require('isomorphic-unfetch')
const htmlToText = require('html-to-text')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()


app.prepare()
.then(() => {
  const server = express()

  server.get('/fetch', async (req, res) => {
    const { url } = req.query
    const errorMessage = `Unable to contact '${url}'`
    const htmlToTextOptions = {
      ignoreHref: true,
      ignoreImage: true
    }

    try {
      const response = await fetch(url)

      if(response.ok) {
        const html = await response.text()
        const text = htmlToText.fromString(html, htmlToTextOptions)

        return res.json({text})
      }
      else {
        return res.status(400).json({errorMessage})
      }
    }
    catch(err) {
      return res.status(400).json({errorMessage})
    }
  })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})