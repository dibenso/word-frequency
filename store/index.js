import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import { ADD_AND_SORT, TOGGLE_TEXT, SWITCH_INPUT_CONTROL,
         KEYBOARD, FETCHING_PAGE, FETCH_PAGE_SUCCESS,
         FETCH_PAGE_FAIL, TOGGLE_COMMON_WORDS } from '../constants'

const initialState = {
  text: '',
  frequencies: {},
  showText: true,
  activeInputControl: KEYBOARD,
  fetchingPage: false,
  fetchPageFailed: false,
  errorMessage: '',
  excludeCommonWords: false
}

export const reducer = (state = initialState, action) => {
  switch(action.type) {
    case ADD_AND_SORT:
      return {...state, frequencies: action.frequencies, text: action.text}
    case TOGGLE_TEXT:
      return {...state, showText: !state.showText}
    case SWITCH_INPUT_CONTROL:
      return {...state, activeInputControl: action.control}
    case FETCHING_PAGE:
      return {...state, fetchingPage: true}
    case FETCH_PAGE_SUCCESS:
      return {...state, fetchingPage: false, fetchPageFailed: false}
    case FETCH_PAGE_FAIL:
      return {...state, fetchingPage: false, fetchPageFailed: true, errorMessage: action.errorMessage}
    case TOGGLE_COMMON_WORDS:
      return {...state, excludeCommonWords: !state.excludeCommonWords}
    default:
      return state
  }
}

export function initializeStore (initialState = initialState) {
  return createStore(reducer, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)))
}
